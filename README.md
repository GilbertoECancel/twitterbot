This Twitter bot takes a certain phrase or hashtag and searches Twitter for the most recent post that contained that specific phrase or hashtag.

Then the bot will retweet and follow all the users that posted the tweet that contained the phrase/hashtag.

The amount of tweets the bot may retweet can be changed.

The bot may also take a specific account and retweet a specific amounts of tweets from that account.